/**
 * Created by Admin on 02.04.2017.
 */
public abstract class Animals {
    private String name;
    private String voice;

    public static final String OUTPUT_FORMAT_LINE
            = "%s say '%s'.";

    protected Animals(String name, String voice) {
        this.name = name;
        this.voice = voice;
    }

    public String getName(){
        return name;
    }

    public String getVoice(){ return voice; }

    public void printDisplay() {
        System.out.println(String.format(
                OUTPUT_FORMAT_LINE,
                name,
                voice
        ));
    }
}