/**
 * Created by Admin on 02.04.2017.
 */
public class Main {
    public static void main(String[] args){
        Cat cat = new Cat();
        Cat cat1 = new Cat("Cat", "Meow-Meow");
        Cat cat2 = new Cat("Barsik", "Meeeow!");

        Cat fox = new Cat();
        Cat fox1 = new Cat("Fox", "Teof-Teof");
        Cat fox2 = new Cat("Foxic", "Teof!");


        cat.printDisplay();
        cat1.printDisplay();
        cat2.printDisplay();

        fox.printDisplay();
        fox1.printDisplay();
        fox2.printDisplay();
    }
}
